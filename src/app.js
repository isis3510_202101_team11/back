//Credeciales de fire base
global.credentials =
  "../Credentials/mini-home-lab-firebase-adminsdk-523d7-a823b37cea.json";
const express = require("express");
const morgan = require("morgan");
const exphds = require("express-handlebars");
const app = express();
const path = require("path");
const index = require("./routes/index");
const profiles = require("./routes/Profile");
const medicalHistory = require("./routes/MedicalHistory");
const Analytics = require("./routes/Analytics");
const Exercises = require("./routes/Exercises");
//const routines = require("./routes/Routines");
const sessions = require("./routes/Session");
const progress = require("./routes/Progress");
const appointment = require("./routes/appointment");
//Settings
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.use(express.json());

//middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true }));
//routes
app.use("/", index);
app.use("/profiles", profiles);
app.use("/medicalHistory", medicalHistory);
app.use("/analytics", Analytics);
app.use("/exercises", Exercises);
app.use("/sessions", sessions);
app.use("/progress", progress);
app.use("/appointment",appointment);
//staticfiles
app.use(express.static(path.join(__dirname, "public")));

//cambiar esto a nodos separados
const tiempoMinutos = [3.4, 5.3, 6, 4.7, 2.8, 1.5, 5];

function prom(list) {
  var suma = 0;
  var resp = 0;
  for (let i = 0; i < list.length; i++) {
    suma += list[i];
  }
  resp = suma / list.length;
  return resp;
}

function min(list) {
  return Math.min.apply(Math, list);
}

function parejas(list) {
  var promMinutos = prom(list);
  var minMinutos = min(list);

  var promSegundos = promMinutos * 60;
  var minSegundos = minMinutos * 60;

  var modProm = promSegundos % 60;
  var modMin = minSegundos % 60;

  modProm = Math.round((modProm + Number.EPSILON) * 100) / 100;
  modMin = Math.round((modMin + Number.EPSILON) * 100) / 100;

  console.log(
    "Tiempo promedios es " +
      Math.floor(promMinutos) +
      " minutos, con " +
      modProm +
      " segundos."
  );
  console.log(
    "Mejor tiempo es " +
      Math.floor(minMinutos) +
      " minutos, con " +
      modMin +
      " segundos."
  );

  return [
    [Math.floor(promMinutos), modProm],
    [Math.floor(minMinutos), modMin],
  ];
}

//app.listen(3000, () => {
//  console.log("Listening on port 3000...");
//parejas(tiempoMinutos);
//})

app.get("/tiempos", function (req, res) {
  res.json({ tiempos: parejas(tiempoMinutos) });
});

function roms(){
  return [[43, 15]]
}

function forces(){
  return [[32, 7]]
}

app.get("/roms", function (req, res) {
  res.json({ roms: roms() });
});

app.get("/forces", function (req, res) {
  res.json({ forces: forces() });
});



module.exports = app;
