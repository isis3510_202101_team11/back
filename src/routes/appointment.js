var express = require("express");
var router = express.Router();
var [getlastAppointmen,addNewAppointment,UpdateAppointment,getNumberOfAppointment] = require("../Controllers/appointmentCtr");

router.get("/number", async function (req, res) {
   const resp = await getNumberOfAppointment();
   let number ={
     NumberOfApointment: parseInt(resp)
   }
   res.send(number);
});

router.get("/:userId", async function (req, res) {
  const resp = await getlastAppointmen(req.params.userId);
  res.send(resp);
});

router.post("/:userId", async function (req, res) {
   const resp = await addNewAppointment(req.params.userId,req.body);
   const apint = req.body;
   res.json(
    {
      CreatDate: `${apint.CreatDate}`,
      Estate: parseInt(apint.Estate),
      DoctorName: `${apint.DoctorName}`,
      Type: apint.Type,
      ModifyDate: `${apint.ModifyDate}`,
      ApointDate: `${apint.ApointDate}`,
    }
   );
});

router.put("/update/:apointmentId", async function (req, res) {
  console.log(req.params.apointmentId);  
  const resp = await UpdateAppointment(req.params.apointmentId,req.body);
  res.json(resp);
});

module.exports = router;
