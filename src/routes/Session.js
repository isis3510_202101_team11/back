var express = require("express");
var router = express.Router();
var [putSessions, getSessions, getSessionsCompleted] = require("../Controllers/SessionCtr");

router.put("/:userId", async function (req, res) {
  let a = JSON.parse(req.body.ejercicio);
  console.log(a);
  const resp = putSessions(req.params.userId, a);
  console.log(resp);
  res.send(resp);
});

router.get("/:userId", async function (req, res) {
  const resp = await getSessions(req.params.userId);
  res.send(resp);
});

router.get("/exercises/:userId", async function (req, res) {
  const resp = await getSessionsCompleted(req.params.userId);
  res.send(resp);
});

module.exports = router;
