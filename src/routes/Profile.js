var express = require("express");
var router = express.Router();
var [getUsers,AddUser,updateLastLogin,updateLogin,GetAllUsers, GetUserLastDays] = require("../Controllers/ProfileCtr")
//

router.get("/",async function (req,res) {

    const resp = await GetAllUsers();
    res.send(resp);
});

router.get("/:userId",async function (req,res) {

    const resp = await getUsers(req.params.userId);
    res.send(resp);
});

router.get("/last/:userId",async function (req,res) {

    const resp = await GetUserLastDays(req.params.userId);
    res.send(resp);
});

router.post("/",async function(req,res,next)
{
    const newUser = await AddUser(req.body);
    res.sendStatus(newUser);
});

router.put("/:userId",async function (req,res){
    const update = await updateLastLogin(req.params.userId,req.body);
    res.send(update);
});

router.put("/login/:userId",async function (req,res){
    const update = await updateLogin(req.params.userId,req.body);
    res.send(update);
});

module.exports = router;