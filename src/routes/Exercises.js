var express = require("express");
var router = express.Router();
var [
  GetAllExercise,
  addExercise,
  getDayRutine,
  getExercise,
  rateExercise,
  GetAllExerciseSmart,
] = require("../Controllers/ExercisesCtr");
var [
  getallMh,
  getMd,
  addMh,
  updateMH,
] = require("../Controllers/medicalHistoryCtr");
router.get("/", async function (req, res) {
  const resp = await GetAllExercise();
  res.send(resp);
});

router.get("/estado", async function (req, res) {
  const resp = await GetAllExerciseSmart();
  res.send(resp);
});

router.get("/Rotine/:userId", async function (req, res) {
  const exer = await GetAllExercise();

  const mh = await getMd(req.params.userId);

  const resp = await getDayRutine(mh, exer);
  res.send(resp);
});

router.get("/:exerciseId", async function (req, res) {
  const resp = await getExercise(req.params.exerciseId);
  res.send(resp);
});

router.post("/:exerciseId", async function (req, res) {
  const resp = await rateExercise(req.params.exerciseId, req.body.rate);
  res.send(resp);
});

router.post("/", async function (req, res, next) {
  const newUser = await addExercise(req.body);
  res.sendStatus(newUser);
});

module.exports = router;
