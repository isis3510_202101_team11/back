var express = require("express");
var router = express.Router();
var [getProgress] = require("../Controllers/ProgressCtr");
//

router.get("/:userId", async function (req, res) {
 // print('El id del usuario es' + req.params.userId);
  const resp = await getProgress(req.params.userId);
  res.send(resp);
});

module.exports = router;
