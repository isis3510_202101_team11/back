var express = require("express");
var router = express.Router();
var [getallMh,getMd,addMh,updateMH] = require("../Controllers/medicalHistoryCtr")

router.get("/",async function (req,res) {

    const resp = await getallMh();
    res.send(resp);
});

router.get("/:userId",async function (req,res) {

    const resp = await getMd(req.params.userId);
    res.send(resp);
});

router.post("/:userId",async function(req,res,next)
{
    const newMh = await addMh(req.params.userId,req.body);
    res.send(newMh);
});

router.put("/:userId",async function (req,res){

    const newMh = await updateMH(req.params.userId,req.body);
    res.send(newMh);
});


module.exports = router;
