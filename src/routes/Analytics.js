var express = require("express");
var router = express.Router();
var [
  getAllAnalytics,
  getLastAnalytic,
  GetAverangeAge,
  GetAverangeInactivity,
  GetFinishPercentage,
  GetAverangeState,
  GetInactiveUsers,
  GetPopularEPS,
  lowerRateExercise,
  HigtesRateExercise,
  GetCommondInjury,
  GetAlternatives,
  CreateNewAnalytics,
] = require("../Controllers/AnalyticsCtr");
var [
  getUsers,
  AddUser,
  updateLastLogin,
  updateLogin,
  GetAllUsers,
] = require("../Controllers/ProfileCtr");
var [
  getallMh,
  getMd,
  addMh,
  updateMH,
] = require("../Controllers/medicalHistoryCtr");
var [
  GetAllExercise,
  addExercise,
  getDayRutine,
  getExercise,
  rateExercise,
] = require("../Controllers/ExercisesCtr");
var [getlastAppointmen,addNewAppointment,UpdateAppointment] = require("../Controllers/appointmentCtr");

var [getlastAppointmen,addNewAppointment,UpdateAppointment,getNumberOfAppointment] = require("../Controllers/appointmentCtr");
/////
router.get("/AllAnalytics", async function (req, res) {
  const resp = await getAllAnalytics();
  res.send(resp);
});
router.get("/lastAnalytic", async function (req, res) {
  const resp = await getLastAnalytic();

  res.send(resp);
});
router.get("/222", async function (req, res) {
 
  const mh = await getallMh();
  const resp = await GetAlternatives(mh);
  console.log(resp);
  res.send(resp);
 
});
router.get("/CreateAnalytic", async function (req, res) {
  let numberOfUser;
  let avergeAge;
  let averageInactivity;
  let finishPercentage;
  let ActiveUsers;
  let averangeState;
  let CurrentDate = new Date();
  let DesertUsers;
  let FInishPercent;
  let DelayDays;
  let commonaflcition;
  let NumberOfapoint;
  let Alterantive;
  //No hay ejericiso definidos para esta metricas


  let LastAnalytic = await getLastAnalytic();
  //NumberOfapoint = 
  const users = await GetAllUsers();
  const mh = await getallMh();
  const Exer = await GetAllExercise();
  let lowesExercise = lowerRateExercise(Exer);
  let HightExercise =HigtesRateExercise(Exer);
  let numberofAppointments = await getNumberOfAppointment();
  console.log(numberofAppointments + "El numero de apointments");
  numberOfUser = users.length;
  avergeAge = GetAverangeAge(users);
  let respose = GetAverangeInactivity(users);
  averageInactivity = respose[0];
  ActiveUsers = respose[1];
  finishPercentage = GetFinishPercentage(users);
  averangeState = GetAverangeState(mh);
  let mhUsers = GetInactiveUsers(mh);
  DelayDays = mhUsers[0];
  FInishPercent = mhUsers[1];
  DesertUsers = mhUsers[2];
  commonaflcition = GetCommondInjury(mh);
  let MoreUsedEps = GetPopularEPS(mh);
  Alterantive = await GetAlternatives(mh);
  let analitycs = await CreateNewAnalytics(
    ActiveUsers,
    avergeAge,
    DelayDays,
    averageInactivity,
    commonaflcition,
    HightExercise,
    lowesExercise,
    MoreUsedEps,
    numberOfUser,
    DesertUsers,
    finishPercentage,
    averangeState,
    FInishPercent,
    numberofAppointments,
    Alterantive
  );

  res.sendStatus(analitycs);
});

module.exports = router;
