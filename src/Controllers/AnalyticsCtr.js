const admin = require("firebase-admin");
const { map } = require("./medicalHistoryCtr");
const serviceAccount = require(global.credentials);
const db = admin.firestore();
const ANALYTICS = "Medic_Analitic";
function getAllAnalytics() {
  try {
    let consult = [];
    return db
      .collection(ANALYTICS)
      .get()
      .then((snapshot) => {
        snapshot.docs.forEach((doc) => {
          consult.push(doc.data());
        });

        return consult;
      });
  } catch (error) {
    console.log(error);
  }
}

function getLastAnalytic() {
  try {
    let consult = [];
    return db
      .collection(ANALYTICS)
      .get()
      .then((snapshot) => {
        snapshot.docs.forEach((doc) => {
          consult.push(doc.data());
        });

        return consult[consult.length - 1];
      });
  } catch (error) {}
}

function GetAverangeAge(Users) {
  let index = 0;
  let total = 0;
  Users.forEach((user) => {
    total += user.Edad;
    index++;
  });

  return total / index;
}

function GetAverangeInactivity(Users) {
  let index = 0;
  let total = 0;
  let temp = 0;
  let Repose = [];
  let Promedio = 0;
  Users.forEach((user) => {
    if (user.hasOwnProperty("lastLogin")) {
      temp =
        new Date().getTime() -
        new Date(user.lastLogin._seconds * 1000).getTime();
      temp = temp / (1000 * 3600 * 24);
    } else {
      temp = 10;
    }

    if (temp < 30) {
      if (temp > 0) {
        total += Math.floor(temp);
      }
      index++;
    }
  });

  Promedio = total / index;
  Repose.push(Promedio);
  Repose.push(index);
  return Repose;
}

function GetFinishPercentage(Users) {
  let index = 0;
  let total = 0;
  Users.forEach((user) => {
    total += user.PorcentajeDeEjerisicios;
    index++;
  });

  return total / index;
}

function GetAverangeState(mh) {
  let index = 0;
  let total = 0;
  mh.forEach((medical) => {
    total += medical.estado;
    index++;
  });

  return total / index;
}

function GetInactiveUsers(mh) {
  let result = [];
  let temp;
  let delayfinishIndex = 0;
  let delayfinishCount = 0;
  let FinishTrathmentCount = 0;
  let FinishTrathmentIndex = 0;
  let DesertUser = 0;
  mh.forEach((medical) => {
    if (medical.Finalizo) {
      temp =
        new Date(medical.fecha_finalizacion._seconds * 1000).getTime() -
        new Date(medical.FechaDeCumplimento._seconds * 1000).getTime();
      temp = temp / (1000 * 3600 * 24);

      if (temp < 0) {
        delayfinishCount += Math.abs(temp);
        FinishTrathmentCount++;
        delayfinishIndex++;
      }
      FinishTrathmentIndex++;
    } else {
      if (medical.hasOwnProperty("fecha_finalizacion")) {
        temp =
          new Date(medical.fecha_finalizacion._seconds * 1000).getTime() -
          new Date().getTime();
        temp = temp / (1000 * 3600 * 24);
      } else {
        temp = 10;
      }
      if (temp < 0) {
        if (Math.abs(temp) >= 30) {
          DesertUser++;
          FinishTrathmentIndex++;
        }
      }
    }
  });
  // usuarios que terminaron el tratamiento con menos de 30 dias despues
  let averangeDelayDays = delayfinishCount / delayfinishIndex;
  //porcentaje de usuarios que terminaron el tratamiento del total
  let porcentFInish = FinishTrathmentCount / FinishTrathmentIndex;
  result.push(averangeDelayDays);
  result.push(porcentFInish);
  result.push(DesertUser);

  return result;
}

function GetPopularEPS(mh) {
  let Epsmap = new Map();
  mh.forEach((medical) => {
    if (Epsmap.has(medical.EPS)) {
      Epsmap.set(medical.EPS, {
        NumberOfPersones:
          parseInt(Epsmap.get(medical.EPS).NumberOfPersones) + 1,
      });
    } else {
      Epsmap.set(medical.EPS, { NumberOfPersones: 1 });
    }
  });

  return Epsmap;
}

function lowerRateExercise(Exer) {
  let name;
  let min = 10000;

  Exer.forEach((exer) => {
    if (exer.UserRate <= min) {
      name = exer.Name;
      min = exer.UserRate;
    }
  });

  return name;
}

function HigtesRateExercise(Exer) {
  let name;
  let max = -1;

  Exer.forEach((exer) => {
    if (exer.UserRate >= max) {
      name = exer.Name;
      max = exer.UserRate;
    }
  });

  return name;
}

function GetCommondInjury(mh) {
  let Epsmap = new Map();

  let name;
  let Max = -1;
  mh.forEach((medical) => {
    if (Epsmap.has(medical.afliccion)) {
      Epsmap.set(medical.afliccion, {
        NumberOfPersones:
          parseInt(Epsmap.get(medical.afliccion).NumberOfPersones) + 1,
      });
    } else {
      Epsmap.set(medical.afliccion, { NumberOfPersones: 1 });
    }
  });

  for (const [key, value] of Epsmap.entries()) {
    if (value.NumberOfPersones >= Max) {
      name = key;
      max = value.NumberOfPersones;
    }
  }

  return name;
}

function GetAlternatives(mh) {
  let alter = new Map();
  mh.forEach((medical) => {
    if (medical.hasOwnProperty("alternativa")) {
      console.log("no tiene");
      if (alter.has(medical.alternativa)) {
        alter.set(medical.alternativa, {
          NumberOfPersones:
            parseInt(alter.get(medical.alternativa).NumberOfPersones) + 1,
        });
      } else {
        alter.set(medical.alternativa, { NumberOfPersones: 1 });
      }
    } else {
      console.log("Si tiene");
      if (alter.has("Niguna")) {
        alter.set("Niguna", {
          NumberOfPersones:
            parseInt(alter.get("Niguna").NumberOfPersones) + 1,
        });
      } else {
        alter.set("Niguna", { NumberOfPersones: 1 });
      }
    }
  });
   console.log(alter);
  return alter;
}

function CreateNewAnalytics(
  ActiveUsers,
  avergeAge,
  DelayDays,
  averageInactivity,
  commonafliction,
  HightExercise,
  lowesExercise,
  moreUsedEps,
  numberOfUser,
  DesertUsers,
  finishPercentage,
  averangeState,
  FInishPercent,
  numberofAppointments,
  Alterantive
) {
  let map = moreUsedEps;
  let map2 =Alterantive;

  const newMedicalAnalytic = {
    ActiveUsers: ActiveUsers,
    AverageAge: avergeAge,
    AverageDelayToFinish: parseInt(DelayDays),
    AverangeUserinactivity: averageInactivity,
    CommonAffliction: commonafliction,
    CurrentAnalyticDate: admin.firestore.Timestamp.fromDate(new Date()),
    HighestRateExercise: HightExercise,
    LowestRateExercise: lowesExercise,
    NumberOfUsers: numberOfUser,
    NumberUserDesert: DesertUsers,
    UserAverageFinishRate: finishPercentage,
    avergueUserState: averangeState,
    percentageFishTreatment: FInishPercent,
    MoreUsedEps: Object.fromEntries(map),
    Alterantive: Object.fromEntries(map2),
    numberofAppointments: parseInt(numberofAppointments),
  };

  try {
    return db
      .collection(ANALYTICS)
      .doc()
      .set(newMedicalAnalytic)
      .then(() => {
        return 201;
      });
  } catch (error) {
    console.log(error);
    return 400;
  }
}

module.exports = [
  getAllAnalytics,
  getLastAnalytic,
  GetAverangeAge,
  GetAverangeInactivity,
  GetFinishPercentage,
  GetAverangeState,
  GetInactiveUsers,
  GetPopularEPS,
  lowerRateExercise,
  HigtesRateExercise,
  GetCommondInjury,
  GetAlternatives,
  CreateNewAnalytics,
];
