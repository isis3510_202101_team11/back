const admin = require("firebase-admin");

const serviceAccount = require(global.credentials);

const db = admin.firestore();
const EXERCISES = "exercises";

function GetAllExercise() {
  try {
    let consult = [];
    return db
      .collection(EXERCISES)
      .get()
      .then((snapshot) => {
        snapshot.docs.forEach((doc) => {
          consult.push(doc.data());
        });

        return consult;
      });
  } catch (error) {
    console.log(error);
  }
}

function GetAllExerciseSmart() {
  try {
    let consult = [];
    var lista = []
    return db
      .collection(EXERCISES)
      .get()
      .then((snapshot) => {
        snapshot.docs.forEach((doc) => {
          consult.push(doc.data());
        });
        
        var dic = {}
        for(let i =0;i<4;i++)
        {
          console.log(consult.length)
          var random = Math.floor(Math.random()*consult.length)
          console.log("random: "+random)
          var id = consult[random].ExerciseId
          dic[i+1] = id
          lista.push(id)
          consult.splice(random,1)
          console.log(id)
        }
        return dic;
      });
  } catch (error) {
    console.log(error);
  }
}

function addExercise(exercise) {
  try {
    return db
      .collection(EXERCISES)
      .doc()
      .set(exercise)
      .then(() => {
        return 200;
      });
  } catch (error) {
    return 401;
  }
}

function rateExercise(exerciseId, rate) {
  if (!isNaN(rate) && (rate > 10 || rate < 0)) {
    throw "Invalid rate value"
  }

  console.log(exerciseId, rate);

  try {
    return db
      .collection(EXERCISES)
      .doc(exerciseId)
      .update({
        TotalRate: admin.firestore.FieldValue.increment(rate),
        RatesNumber: admin.firestore.FieldValue.increment(1),
      })
      .then(() => {
        return 200;
      });
  } catch (error) {
    console.log(error);
    return 400;
  }
}

function getDayRutine(mh, excer) {
  let MaximumLevel = 10;
  let NumberOfExer = 6;
  let Rutine = [];
  let complte = false;
  let numfExer = 0;
  console.log(mh.estado == 4);
  if (mh.estado <= 2) {
    NumberOfExer = 2;
    if (mh.estado <= 1) {
      MaximumLevel = 3;
    } else {
      MaximumLevel = 5;
    }
  } else if (mh.estado == 3) {
    NumberOfExer = 3;
    MaximumLevel = 6;
  } else if (mh.estado == 4) {
    MaximumLevel = 8;
  }
  console.log("Maximo nivel" + MaximumLevel);
  console.log("Numero de Ejercicios" + NumberOfExer);
  while (!complte) {
    console.log(NumberOfExer);

    excer.forEach((exer) => {
      let esDelTipo = false;

      if (numfExer < NumberOfExer) {
        let i;
        for (i = 0; i < exer.Tipo.length && !esDelTipo; i++) {
          if (exer.Tipo[i] == mh.afliccion) {
            esDelTipo = true;
          }
        }
        if (esDelTipo) {
          if (exer.Dificultad <= MaximumLevel) {
            Rutine.push(exer);
            numfExer++;
          }
        }
      }
    });

    if (numfExer > 0 && numfExer < NumberOfExer) {
      NumberOfExer = NumberOfExer / 2;
    } else if (numfExer == 0) {
      MaximumLevel++;
      if (MaximumLevel >= 10) {
        complte = true;
      }
    } else if (numfExer >= NumberOfExer) {
      complte = true;
    }
  }
  return Rutine;
}

function getExercise(exerciseId) {
  console.log(exerciseId);
  try {
    return db
      .collection("exercises")
      .doc(exerciseId)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          return "El ejercicio no existe";
        } else {
          let exercise = doc.data();
          if (exercise.TotalRate && exercise.RatesNumber) {
            exercise.UserRate = +(exercise.TotalRate / exercise.RatesNumber).toFixed(2);
          }

          return exercise;
        }
      });
  } catch (error) {
    console.log(error);
  }
}

module.exports = [GetAllExercise, addExercise, getDayRutine, getExercise, rateExercise, GetAllExerciseSmart];
