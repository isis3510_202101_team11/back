const admin = require("firebase-admin");

const serviceAccount = require(global.credentials);

const db = admin.firestore();
const Appointment = "Appointment";
const UserApint = "UserAppointment";
function getlastAppointmen(userId) {
  try {
    let consult;
    return db
      .collection(UserApint)
      .doc(userId)
      .get()
      .then((snapshot) => {
        let index = snapshot.data().appointment.length - 1;
        let apoint = snapshot.data().appointment[index];
        return db
          .collection(Appointment)
          .doc(apoint)
          .get()
          .then((snapshot) => {
            snapshot.data();
            console.log(consult);
            consult = {
              CreatDate: snapshot.data().CreatDate,
              Estate: snapshot.data().Estate,
              DoctorName: snapshot.data().DoctorName,
              Type: snapshot.data().Type,
              ModifyDate: snapshot.data().ModifyDate,
              ApointDate: snapshot.data().ApointDate,
              appointmentId: apoint,
            };
            return consult;
          });
      });
  } catch (error) {
    console.log(error);
  }
}

function addNewAppointment(userid, apint) {
  try {
    const docref = db.collection(Appointment).doc();
    const docId = docref.id;

    return docref
      .set({
        CreatDate: `${apint.CreatDate}`,
        Estate: parseInt(apint.Estate),
        DoctorName: `${apint.DoctorName}`,
        Type: apint.Type,
        ModifyDate: `${apint.ModifyDate}`,
        ApointDate: `${apint.ApointDate}`,
      })
      .then(() => {
        return db
          .collection(UserApint)
          .doc(userid)
          .get()
          .then((snapshot) => {
            if(snapshot.data()==null)
            {
              appointment  = [];
              appointment.push(docId);
              return db
              .collection(UserApint)
              .doc(userid)
              .set(
                {
                  appointment: appointment,
                }
              ).then(
                ()=>{
                  return 200;
                }
              )
         
            }
            else
            {
              let arra = snapshot.data().appointment;
              arra.push(docId);
              return db
                .collection(UserApint)
                .doc(userid)
                .set({
                  appointment: arra,
                })
                .then(() => {
                  console.log(arra);
  
                  return 200;
                });
            }

    
          });
      });
  } catch (error) {
    console.log(error);
    return 401;
  }
}

function UpdateAppointment(appintId, update) {

  try {
   
    var UpdateAppont = {
        Estate: parseInt(update.Estate),
        Type: update.Type,
        ModifyDate: `${new Date()}`,
        ApointDate: `${update.ApointDate}`,
      };
      console.log(UpdateAppont);
    return db
      .collection(Appointment)
      .doc(appintId)
      .update(update).then((data)=>{
        console.log(data);  
        return db.collection(Appointment).doc(appintId).get().then((snapshot) => {
            const consult = {
              CreatDate: snapshot.data().CreatDate,
              Estate: snapshot.data().Estate,
              DoctorName: snapshot.data().DoctorName,
              Type: snapshot.data().Type,
              ModifyDate: snapshot.data().ModifyDate,
              ApointDate: snapshot.data().ApointDate,
              appointmentId: appintId,
            };
            return consult;
        }); 
      });
    
  } catch (error) {
      console.log(error);
    return 401;
  }
}

function getNumberOfAppointment()
{
  return db
      .collection(Appointment)
      .get().then((snapshot) =>{
      
        return snapshot.docs.length;
      });
}
module.exports = [getlastAppointmen, addNewAppointment, UpdateAppointment,getNumberOfAppointment];
