const admin = require("firebase-admin");
const serviceAccount = require(global.credentials);
if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });
}
const db = admin.firestore();

function putSessions(userId, newSes) {
  let user = userId;
  try {
    const ref = db.collection("sessions").doc(user);
    ref
    .update({
      sessions: admin.firestore.FieldValue.arrayUnion(newSes),
    })
    .then((e) => {
      postProgress(userId, newSes);
      console.log(e);
      return e;
    })
    .catch((error) => {
      db.collection("sessions").doc(user).set({ "sessions": newSes });
      return postProgress(userId, newSes);
    })
    .catch ((e) => {
      console.log(e);
      return "Error";
    });
  } catch (error) {
    console.log(error);
  }
}

function getSessions(userId) {
  let user = userId;
  console.log(user);
  try {
    return db
      .collection("sessions")
      .doc(user)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          return "El ejercicio no existe";
        } else {
          console.log(doc.fieldsProto);
          console.log(doc.data());
          return doc.data();
        }
      });
  } catch (error) {
    console.log(error);
  }
}
//get excercises completed 
function getSessionsCompleted(userId) {
  let user = userId;
  console.log(user);
  try {
    return db
      .collection("sessions")
      .doc(user)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          return "El ejercicio no existe";
        } else {
          var data = doc.data().sessions
          var data2 = []
          if(!Array.isArray(data))
          {
            for(var i in data){
              data2.push(data[i])
            }
            data=data2
          }
          console.log(data)
          contador_true = 0
          contador_false = 0
          for(let i = 0; i < data.length;i++)
          {
            var completed = data[i].completed
            if(completed != null && completed == true)
            {
              contador_true++
              console.log(completed)
            }
            else if(completed != null && completed == false) 
            {
              contador_false++
              console.log(completed)
            }
          }
          console.log("numero True: "+contador_true)
          console.log("numero False: "+ contador_false)
          dic = {"numCompleted":contador_true, "numNotCompleted":contador_false}
          //console.log(data);
          return dic;
        }
      });
  } catch (error) {
    console.log(error);
  }
}
async function postProgress(userId, sessionObject) {
  const {
    completed = false,
    measures = [{ t: 0, x: 0, y: 0, z: 0 }],
    date = 1,
  } = sessionObject;

  let sessions = [];
  try {
    const ref = db.collection("sessions").doc(userId);
    await ref.get().then((response) => {
      sessions = response.data();
    });
  } catch {

  }

  let dataRomDays = [];
  let dataFuerzaDays = [];
  let dataTiempoDays = [];

  sessions.forEach((session) => {
    if (session.measures) {
      session.measures.forEach((measure, index) => {
        // TIEMPO
        const valueT = measure.t || 0;
        const date = session.date;
        dataTiempoDays.push({ x: date, y: valueT });

        if (index == 0) {
          // ROM
          const valueRom =
            (measure.x || 1) + (measure.y || 1) + (measure.z || 1);
          dataRomDays.push({ x: date, y: valueRom });

          // FUERZA
          const valueFuerza =
            (measure.x || 1) + (measure.y || 1) + (measure.z || 1);
          dataFuerzaDays.push({ x: date, y: valueFuerza });
        } else {
          const valueRomPrev =
            (measures[index - 1].x || 1) +
            (measures[index - 1].y || 1) +
            (measures[index - 1].z || 1);
          const valueRomActual =
            (measure.x || 1) +
            (measure.y || 1) +
            (measure.z || 1) +
            valueRomPrev;
          dataRomDays.push({ x: date, y: valueRomActual });

          const valueFuerzaPrev =
            (measures[index - 1].x || 1) +
            (measures[index - 1].y || 1) +
            (measures[index - 1].z || 1);
          const valueFuerzaActual =
            ((measure.x || 1) + (measure.y || 1) + (measure.z || 1)) *
            valueFuerzaActual;
          dataFuerzaDays.push({ x: date, y: valueFuerzaPrev });
        }
      });
    }
  });

  // 7 days
  let dataRomWeek = [{ week: new Date(), days: [] }];
  let dataFuerzaWeek = [{ week: new Date(), days: [] }];
  let dataTiempoWeek = [{ week: new Date(), days: [] }];

  let cont = 1;
  let weekDays = [];
  let weekDate = new Date();
  dataRomDays.forEach((rom, index) => {
    if (cont == 1) {
      weekDate = rom.x;
    }

    if (cont == 7) {
      cont = 1;
      weekDays = [rom];
    } else {
      weekDays.push(rom);
    }

    dataRomWeek.push({ week: weekDate, days: weekDays });
    cont += 1;
  });

  cont = 1;
  weekDays = [];
  weekDate = new Date();
  dataFuerzaDays.forEach((fuerza, index) => {
    if (cont == 1) {
      weekDate = fuerza.x;
    }

    if (cont == 7) {
      cont = 1;
      weekDays = [fuerza];
    } else {
      weekDays.push(fuerza);
    }

    dataFuerzaWeek.push({ week: weekDate, days: weekDays });
    cont += 1;
  });

  cont = 1;
  weekDays = [];
  weekDate = new Date();
  dataTiempoDays.forEach((tiempo, index) => {
    if (cont == 1) {
      weekDate = tiempo.x;
    }

    if (cont == 7) {
      cont = 1;
      weekDays = [tiempo];
    } else {
      weekDays.push(tiempo);
    }

    dataTiempoWeek.push({ week: weekDate, days: weekDays });
    cont += 1;
  });

  /**
   * MESES
   */

  // 7 days
  let dataRomMonth = [{ month: new Date(), days: [] }];
  let dataFuerzaMonth = [{ month: new Date(), days: [] }];
  let dataTiempoMonth = [{ month: new Date(), days: [] }];

  cont = 1;
  let monthDays = [];
  let monthDate = new Date();
  dataRomDays.forEach((rom, index) => {
    if (cont == 1) {
      monthDate = rom.x;
    }

    if (cont == 30) {
      cont = 1;
      monthDays = [rom];
    } else {
      monthDays.push(rom);
    }

    dataRomMonth.push({ month: monthDate, days: monthDays });
    cont += 1;
  });

  cont = 1;
  monthDays = [];
  monthDate = new Date();
  dataFuerzaDays.forEach((fuerza, index) => {
    if (cont == 1) {
      monthDate = fuerza.x;
    }

    if (cont == 30) {
      cont = 1;
      monthDays = [fuerza];
    } else {
      monthDays.push(fuerza);
    }

    dataFuerzaMonth.push({ month: monthDate, days: monthDays });
    cont += 1;
  });

  cont = 1;
  monthDays = [];
  monthDate = new Date();
  dataTiempoDays.forEach((tiempo, index) => {
    if (cont == 1) {
      monthDate = tiempo.x;
    }

    if (cont == 30) {
      cont = 1;
      monthDays = [tiempo];
    } else {
      monthDays.push(tiempo);
    }

    dataTiempoMonth.push({ month: monthDate, days: monthDays });
    cont += 1;
  });

  /**
   * Destacados
   */

  // Rom
  const RomMonthDays = [...dataRomMonth.map((x) => x.days)];
  const RomMax = Math.max(...RomMonthDays.map((item) => item.y));
  const RomItem = RomMonthDays.find((item) => item.y == RomMax);

  const destacadoRomMonth = { month: RomItem.x, value: RomMax };

  const RomWeekDays = [...dataRomWeek.map((x) => x.days)];
  const RomMaxWeek = Math.max(...RomWeekDays.map((item) => item.y));
  const RomItemWeek = RomWeekDays.find((item) => item.y == RomMaxWeek);

  const destacadoRomWeek = {
    week: RomItemWeek.x,
    value: RomMaxWeek,
  };

  // Fuerza

  const FuerzaMonthDays = [...dataFuerzaMonth.map((x) => x.days)];
  const FuerzaMax = Math.max(...FuerzaMonthDays.map((item) => item.y));
  const FuerzaItem = FuerzaMonthDays.find((item) => item.y == FuerzaMax);

  const destacadoFuerzaMonth = { month: FuerzaItem.x, value: FuerzaMax };

  const FuerzaWeekDays = [...dataFuerzaWeek.map((x) => x.days)];
  const FuerzaMaxWeek = Math.max(...FuerzaWeekDays.map((item) => item.y));
  const FuerzaItemWeek = FuerzaWeekDays.find((item) => item.y == FuerzaMaxWeek);

  const destacadoFuerzaWeek = {
    week: FuerzaItemWeek.x,
    value: FuerzaMaxWeek,
  };

  // Tiempo
  const tiempoMonthDays = [...dataTiempoMonth.map((x) => x.days)];
  const tiempoMax = Math.max(...tiempoMonthDays.map((item) => item.y));
  const tiempoItem = tiempoMonthDays.find((item) => item.y == tiempoMax);

  const destacadoTiempoMonth = { month: tiempoItem.x, value: tiempoMax };

  const tiempoWeekDays = [...dataTiempoWeek.map((x) => x.days)];
  const tiempoMaxWeek = Math.max(...tiempoWeekDays.map((item) => item.y));
  const tiempoItemWeek = tiempoWeekDays.find((item) => item.y == tiempoMaxWeek);

  const destacadoTiempoWeek = { week: tiempoItemWeek.x, value: tiempoMaxWeek };

  /**
   * DB
   */

  const col = db.collection("progress");
  col
    .add({
      userId,

      dataRomWeek,
      dataRomMonth,
      destacadosRomWeek: destacadoRomWeek,
      destacadosRomMonth: destacadosRomMonth,

      dataFuerzaWeek,
      dataFuerzaMonth,
      destacadosFuerzaWeek: destacadoFuerzaWeek,
      destacadosFuerzaMonth: destacadoFuerzaMonth,

      dataTiempoRWeek,
      dataTiempoRMonth,
      destacadosTiempoRWeek: destacadoTiempoWeek,
      destacadosTiempoRMonth: destacadoTiempoMonth,
    })
    .then((responseProgress) => {
      console.log("Progress object", responseProgress);
    });
}



module.exports = [putSessions, getSessions, getSessionsCompleted];
