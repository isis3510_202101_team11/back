const admin = require("firebase-admin");

const serviceAccount = require(global.credentials);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});
const db = admin.firestore();
const PROFILES = "profile";

function GetAllUsers() {
  try {
    let consult = [];
    return db
      .collection(PROFILES)
      .get()
      .then((snapshot) => {
        snapshot.docs.forEach((doc) => {
          consult.push(doc.data());
        });

        return consult;
      });
  } catch (error) {
    console.log(error);
  }
}

function GetUser(userid) {
  let user = userid;
  try {
    return db
      .collection(PROFILES)
      .doc(user)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          return "No existe eso";
        } else {
          return doc.data();
        }
      });
  } catch (error) {
    console.log(error);
  }
}

function GetUserLastDays(userid) {
  let user = userid;
  try {
    return db
      .collection(PROFILES)
      .doc(user)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          return "No existe eso";
        } else {
          
          if(doc.data().lastLogin != null){
            var nombre = doc.data().Name
            var ultimo = doc.data().lastLogin.toDate()
            var hoy = new Date()
            console.log(nombre)
            console.log(ultimo)
            console.log(hoy)
            // To calculate the time difference of two dates
            var Difference_In_Time = hoy.getTime() - ultimo.getTime();
    
            // To calculate the no. of days between two dates
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            var rounded = Math.floor(Difference_In_Days)
            console.log("Ultimo ejercicio realizado hace "+rounded+" dias")
            var dic = {"lastDaysCount":rounded}
            return dic;
          }
          else return "Last login date no existe para ese usuario"
          
        }
      });
  } catch (error) {
    console.log(error);
  }
}

function AddUser(newUser) {
  const user = {
    Edad: calculateAge(new Date(newUser.FechaNacimiento)),
    FechaNacimiento: admin.firestore.Timestamp.fromDate(
      new Date(newUser.FechaNacimiento)
    ),
    Name: newUser.Name,
    ProfilePic: newUser.ProfilePic,
    lastLogin: admin.firestore.Timestamp.fromDate(new Date(newUser.lastLogin)),
    roll: "user",
    PorcentajeDeEjerisicios: 0.5,
  };

  return db
    .collection(PROFILES)
    .doc(newUser.uId)
    .set(user)
    .then(() => {
      return 200;
    });
}

function updateLastLogin(userid, UpdateLogin) {
  try {
    return db.collection("profile").doc(userid).update(UpdateLogin);
  } catch (error) {
    return error;
  }
}
function updateLogin(userId, update) {
  try {
    return db.collection("profile").doc(userId).update(update);
  } catch (error) {
    return error;
  }
}

function calculateAge(birthdayt) {
  var diff_ms = Date.now() - birthdayt.getTime();
  var age_dt = new Date(diff_ms);
  return Math.abs(age_dt.getUTCFullYear() - 1970);
}

module.exports = [GetUser, AddUser, updateLastLogin, updateLogin, GetAllUsers, GetUserLastDays];
