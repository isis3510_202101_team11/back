const admin = require("firebase-admin");
const serviceAccount = require(global.credentials);
if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });
}
const db = admin.firestore();

function getProgress(userId) {
  try {
    return db
      .collection("progress")
      .where("userId","==",userId)
      .get()
      .then((snapshot) => {
        console.log(snapshot.docs.map((doc) => doc.data()));
        return snapshot.docs.map((doc) => doc.data())[0];
      });
  } catch (error) {
    console.log(error);
  }

return {};
}

module.exports = [getProgress];
