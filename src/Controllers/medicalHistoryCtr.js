const admin = require("firebase-admin");
const serviceAccount = require(global.credentials);

const db = admin.firestore();

function getallMh() {
  try {
    let consult = [];
    return db
      .collection("historial_medico")
      .get()
      .then((snapshot) => {
        snapshot.docs.forEach((doc) => {
          consult.push(doc.data());
        });

        return consult;
      });
  } catch (error) {
    console.log(error);
  }
}

function getMd(userid) {
  let user = userid;
  try {
    return db
      .collection("historial_medico")
      .doc(userid)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          return "No existe eso";
        } else {
          
          return doc.data();
        }
      });
  } catch (error) {
    console.log(error);
  }
}
function addMh(userid, newMh) {
  console.log(newMh);
  const Mh = {
    EPS: newMh.EPS,
    Finalizo: newMh.Finalizo == "true" ? true:false,
    afliccion: newMh.afliccion,
    estado: parseInt(newMh.estado)  ,
    FechaDeCumplimento: admin.firestore.Timestamp.fromDate(
      new Date()),
    fecha_finalizacion: admin.firestore.Timestamp.fromDate(
      new Date(newMh.fecha_finalizacion)
    ),
    inicio: admin.firestore.Timestamp.fromDate(new Date(newMh.inicio)),
    alternativa: newMh.hasOwnProperty("alternativa")? newMh.alternativa:"Ninguna"
  };

  return db.collection("historial_medico").doc(userid).set(Mh);
}

function updateMH(userId, mh) {
  var imp = {
    estado: Number(mh.estado),
  };
  try {
    return db.collection("historial_medico").doc(userId).update(imp);
  } catch (error) {
    return error;
  }
}

module.exports = [getallMh, getMd, addMh, updateMH];
